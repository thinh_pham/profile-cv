import React from "react"
import { ContactWrapper, HeaderSection, Image, Info, InfoLeft, InfoRight } from "./styles"
import { GridContainer, Col } from "../../utils/grid"
import MapImage from "../../assets/images/maps.png"
import Form from "../../components/Form"

export default function Contact() {
	return (
		<ContactWrapper>
			<HeaderSection>
				<h3 className="title-section">Liên hệ</h3>
			</HeaderSection>
			<GridContainer style={{ maxWidth: "110rem" }}>
				<Col rStart={1} cStart={1} rEnd={1} cEnd={7}>
					<Image src={MapImage} />
					<Info>
						<InfoLeft>
							<p>
								665/64/12 Đường số 17A, Bình Hưng Hòa A,
								<br />
								Bình Tân, Tp. Hồ Chí Minh
							</p>
						</InfoLeft>
						<InfoRight>
							<p>Gọi cho tôi : +01 111 222 3333</p>
							<p>
								E-mail : <a href="#">mail@example.com</a>
							</p>
						</InfoRight>
					</Info>
				</Col>
				<Col rStart={1} cStart={7} rEnd={1} cEnd={13}>
					<Form />
				</Col>
			</GridContainer>
		</ContactWrapper>
	)
}
