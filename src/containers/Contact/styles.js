import styled from "styled-components"

export const ContactWrapper = styled.div`
	padding: 8rem 0;
	height: 100vh;
	background-color: rgba(0, 0, 0, 1);
`

export const HeaderSection = styled.div`
	max-width: 115rem;
	margin: 0 auto;
`

export const Image = styled.img`
	width: 54rem;
`

export const Info = styled.div`
	margin-top: 2rem;
	border: 0.1rem solid #fff;
	padding: 2rem 3rem;
	display: flex;
	justify-content: space-between;
	& p {
		font-size: 1.4rem;
		font-weight: 300;
		line-height: 2.5rem;
		color: #ccc;
	}
`

export const InfoLeft = styled.div`
	width: 49%;
	position: relative;
	&::after {
		content: "";
		width: 1px;
		height: 100%;
		background-color: #ccc;
		position: absolute;
		top: 0;
		right: 0;
	}
	& > p {
		text-align: left;
	}
`

export const InfoRight = styled.div`
	& > p > a {
		text-decoration: none;
		color: #ccc;
		transition: all 0.2s;
		&:hover {
			color: #f58700;
		}
	}
`
