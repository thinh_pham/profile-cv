import React from "react"
import { Col } from "../../utils/grid"
import { WorkWrapper } from "./styles"
import Exp_Edu from "../../components/Exp_Edu"
import Projects from "../../components/Projects"
import g1 from "../../assets/images/projects/g1.jpg"
import g2 from "../../assets/images/projects/g2.jpg"
import g3 from "../../assets/images/projects/g3.jpg"
import g4 from "../../assets/images/projects/g4.jpg"
import g5 from "../../assets/images/projects/g5.jpg"
import g6 from "../../assets/images/projects/g6.jpg"

const works = {
	experience: [
		{
			id: 1,
			title: "2018 - 2021",
			panelTitle: "Tên công ty",
			panelParagraph:
				"Porro quisquam est neque, qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia.",
		},
		{
			id: 2,
			title: "2021 - 2023",
			panelTitle: "Tên công ty",
			panelParagraph:
				"Porro quisquam est neque, qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia.",
		},
	],
	education: [
		{
			id: 1,
			title: "Degree (2010-2012)",
			panelTitle: "University Name",
			panelParagraph:
				"Porro quisquam est neque, qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia.",
		},
	],
	projects: [
		{ id: 1, name: "My Project", img: g1 },
		{ id: 2, name: "My Project", img: g2 },
		{ id: 3, name: "My Project", img: g3 },
		{ id: 4, name: "My Project", img: g4 },
		{ id: 5, name: "My Project", img: g5 },
		{ id: 6, name: "My Project", img: g6 },
	],
}

export default function Work() {
	const { experience, education, projects } = works

	return (
		<WorkWrapper style={{ padding: "8rem 0" }}>
			<Col style={{ placeSelf: "stretch" }} rStart={1} cStart={1} rEnd={1} cEnd={7}>
				<Exp_Edu title="Kinh nghiệm" accordions={experience} />
				<div style={{ margin: "5rem 0" }}></div>
				<Exp_Edu title="Học vấn" accordions={education} />
			</Col>
			<Col
				style={{ placeSelf: "stretch", marginLeft: "5rem" }}
				rStart={1}
				cStart={7}
				rEnd={1}
				cEnd={13}>
				<Projects projects={projects} />
			</Col>
		</WorkWrapper>
	)
}
