import styled from "styled-components"
import { GridContainer } from "../../utils/grid"

export const WorkWrapper = styled(GridContainer)`
	max-width: 110.4rem;
`
