import React from "react"
import { GridContainer, Col } from "../../utils/grid"
import {
	SkillsWrapper,
	HeaderSection,
	Skillbar,
	SkillbarTitle,
	SkillbarPercent,
} from "./styles"

export default function Skills() {
	return (
		<SkillsWrapper>
			<HeaderSection>
				<h3 className="title-section">Kĩ năng</h3>
				<p className="paragraph-section">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse laoreet sem
					sit amet dolor luctus pellentesque. Pellentesque eleifend tellus at interdum
					elementum. Nam egestas molestie elit. Vivamus sed accumsan quam, a mollis magna.
					Nam aliquet eros eget sapien consequat tincidunt at vel nibh. Duis ut turpis mi.
					Duis nec scelerisque urna, sit amet varius arcu. Aliquam aliquet sapien quis
					mauris semper suscipit. Maecenas pharetra dapibus posuere. Praesent odio sem,
					varius quis dolor vel, maximus dapibus mi. Pellentesque mattis mauris neque. Nam
					aliquam turpis ante, at cursus massa ullamcorper ut. Proin id diam id nisi
					sagittis pellentesque sed sit amet eros.
				</p>
			</HeaderSection>
			<GridContainer style={{ marginTop: "6rem" }}>
				<Col style={{ justifySelf: "stretch" }} rStart={1} cStart={2} rEnd={1} cEnd={7}>
					<Skillbar percent={100} color="#d35400bf">
						<SkillbarTitle color="#d35400">html5</SkillbarTitle>
						<SkillbarPercent>100%</SkillbarPercent>
					</Skillbar>
					<Skillbar percent={75} color="#d35400bf">
						<SkillbarTitle color="#d35400bf">css3</SkillbarTitle>
						<SkillbarPercent>75%</SkillbarPercent>
					</Skillbar>
					<Skillbar percent={60} color="#d35400bf">
						<SkillbarTitle color="#d35400bf">javascript</SkillbarTitle>
						<SkillbarPercent>60%</SkillbarPercent>
					</Skillbar>
				</Col>
			</GridContainer>
		</SkillsWrapper>
	)
}
