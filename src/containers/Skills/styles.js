import styled from "styled-components"

export const SkillsWrapper = styled.div`
	margin: 0;
	padding: 10rem 0;
	height: 100vh;
	background: url(${(props) => props.backgroundImage}) no-repeat center center;
	background-size: cover;
`

export const HeaderSection = styled.div`
	max-width: 92.4rem;
	margin: 0 auto;
`

export const Skillbar = styled.div`
	background-color: #fff;
	height: 3rem;
	line-height: 3rem;
	border-radius: 0.3rem;
	overflow: hidden;
	position: relative;
	&:not(:first-child) {
		margin-top: 3rem;
	}
	&::before {
		content: "%";
		width: ${(props) => props.percent || ""}%;
		background-color: ${(props) => props.color};
		top: 0;
		left: 0;
		position: absolute;
		text-align: left;
	}
`

export const SkillbarTitle = styled.span`
	text-transform: uppercase;
	font-size: 1.4rem;
	padding: 0 2rem;
	background-color: ${(props) => props.color};
	z-index: 99999;
	position: absolute;
`

export const SkillbarPercent = styled.span`
	text-transform: uppercase;
	font-size: 1.2rem;
	font-weight: bold;
	padding: 0 2rem;
	right: 0;
	position: absolute;
`
