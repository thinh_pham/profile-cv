import React from "react"
import { FaFacebookF, FaTwitter, FaGooglePlusG, FaGithub } from "react-icons/fa"
import { GridContainer, Col } from "../../utils/grid"
import { BannerInfo, BannerTitle } from "./styles.js"
import PersonalImage from "../../assets/images/img.jpg"
// import BackgroundImage from "../../assets/images/slide1.jpg"
import Avatar_Social from "../../components/Avatar_Social"
import About from "../../components/About"

const socials = [
	{
		id: 1,
		name: "Facebook",
		href: "https://facebook.com",
		bgColor: "#3b5998",
		icon: <FaFacebookF />,
	},
	{
		id: 2,
		name: "Twitter",
		href: "https://twitter.com",
		bgColor: "#4099ff",
		icon: <FaTwitter />,
	},
	{
		id: 3,
		name: "Google Plus",
		href: "https://google.com",
		bgColor: "#d34836",
		icon: <FaGooglePlusG />,
	},
	{
		id: 4,
		name: "Github",
		href: "https://github.com",
		bgColor: "#2ea44f",
		icon: <FaGithub />,
	},
]

export default function Banner() {
	return (
		<BannerInfo>
			<BannerTitle>CV</BannerTitle>
			<GridContainer>
				<Col rStart={1} cStart={1} rEnd={1} cEnd={4}>
					<Avatar_Social personalImage={PersonalImage} socials={socials} />
				</Col>
				<Col rStart={1} cStart={5} rEnd={1} cEnd={13} style={{ alignSelf: "stretch" }}>
					<About />
				</Col>
			</GridContainer>
		</BannerInfo>
	)
}
