import styled from "styled-components"

export const BannerInfo = styled.div`
	margin: 0;
	padding-top: 2rem;
	height: 100vh;
	background: url(${(props) => props.backgroundImage}) no-repeat center center;
	background-color: rgba(0, 0, 0, 0.7);
`

export const BannerTitle = styled.h1`
	text-align: center;
	color: #fff;
	font-size: 6rem;
	font-weight: 200;
`
