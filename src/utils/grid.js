import styled from "styled-components"

export const GridContainer = styled.div`
	max-width: 126.4rem;
	margin: 0 auto;
	display: grid;
	grid-template-columns: repeat(12, 1fr);
	gap: 1rem 1rem;
`

export const Col = styled.div`
	place-self: center;
	grid-area: ${(props) =>
		props.rStart && props.cStart && props.rEnd && props.cEnd
			? props.rStart + "/" + props.cStart + "/" + props.rEnd + "/" + props.cEnd
			: "auto / auto / auto / auto"};
`
