import React from "react"
import Banner from "./containers/Banner"
import Skills from "./containers/Skills"
import Work from "./containers/Work"
import Contact from "./containers/Contact"

function App() {
	return (
		<>
			<Banner />
			<Skills />
			<Work />
			<Contact />
		</>
	)
}

export default App
