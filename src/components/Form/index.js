import React from "react"
import PropTypes from "prop-types"
import { reduxForm } from "redux-form"

Form.propTypes = {
	fields: PropTypes.array,
	handleSubmit: PropTypes.func.isRequired,
	resetForm: PropTypes.func.isRequired,
	submitting: PropTypes.func.isRequired,
}

Form.defaultProps = {
	fields: [],
}

function Form(props) {
	const {
		fields: { firstName, lastName, email, employed, notes },
		handleSubmit,
		resetForm,
		submitting,
	} = props

	return (
		<form onSubmit={handleSubmit}>
			<div>
				<label>First Name</label>
				<div>
					<input type="text" placeholder="First Name" {...firstName} />
				</div>
			</div>
			<div>
				<label>Last Name</label>
				<div>
					<input type="text" placeholder="Last Name" {...lastName} />
				</div>
			</div>
			<div>
				<label>Email</label>
				<div>
					<input type="email" placeholder="Email" {...email} />
				</div>
			</div>
			<div>
				<label>
					<input type="checkbox" {...employed} /> Employed
				</label>
			</div>
			<div>
				<label>Notes</label>
				<div>
					<textarea {...notes} value={notes || ""} />
				</div>
			</div>
			<div>
				<button type="submit" disabled={submitting}>
					{submitting ? <i /> : <i />} Submit
				</button>
				<button type="button" disabled={submitting} onClick={resetForm}>
					Clear Values
				</button>
			</div>
		</form>
	)
}

export default reduxForm({
	form: "contact",
	fields: ["firstName", "lastName", "email", "employed", "notes"],
})(Form)
