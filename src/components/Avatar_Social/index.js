import React from "react"
import PropTypes from "prop-types"
import { Image, Info, Fullname, SocialIcons, SocialItem, Icon } from "./styles"

Avatar_Social.propTypes = {
	personalImage: PropTypes.string,
	socials: PropTypes.array,
}

Avatar_Social.defaultProps = {
	personalImage: "",
	socials: [],
}

export default function Avatar_Social(props) {
	const { personalImage, socials } = props

	return (
		<>
			<Image src={personalImage} alt="Avatar" />
			<Info>
				<Fullname>Phạm Quốc Thịnh</Fullname>
				<SocialIcons>
					{socials.map((social) => {
						const { id, name, href, bgColor, icon } = social
						return (
							<SocialItem key={id}>
								<Icon href={href} bgColor={bgColor} alt={name}>
									{icon}
								</Icon>
							</SocialItem>
						)
					})}
				</SocialIcons>
			</Info>
		</>
	)
}
