import styled from "styled-components"

export const Image = styled.img`
	width: 30rem;
	height: 30rem;
	border: 0.3rem solid #fff;
`

export const Info = styled.div`
	width: 30rem;
	padding: 2rem 0;
	border: 0.3rem solid #fff;
`

export const Fullname = styled.h3`
	color: #fff;
	font-size: 2.4rem;
	font-weight: 300;
	text-align: center;
	text-transform: capitalize;
`

export const SocialIcons = styled.ul`
	text-align: center;
	margin-top: 2rem;
`

export const SocialItem = styled.li`
	list-style: none;
	display: inline-block;
`

export const Icon = styled.a`
	background-color: ${(props) => props.color || "transparent"};
	margin: 0 0.8rem;
	padding: 1.2rem;
	font-size: 1.8rem;
	color: #fff;
	display: flex;
	transition: all 0.3s ease-in-out;
	box-shadow: inset 0 0 0 2rem ${(props) => props.bgColor || "transparent"};
	&:hover {
		box-shadow: inset 0 0 0 0.2rem ${(props) => props.bgColor || "transparent"};
	}
`
