import React from "react"
import PropTypes from "prop-types"
import Accordion from "../Accordion"

Exp_Edu.propTypes = {
	title: PropTypes.string,
	accordions: PropTypes.array,
}

Exp_Edu.defaultProps = {
	title: "",
	accordions: [],
}

export default function Exp_Edu(props) {
	const { title, accordions } = props
	return (
		<>
			<h3 className="title-section">{title}</h3>
			<Accordion accordions={accordions} />
		</>
	)
}
