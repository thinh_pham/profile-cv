import styled from "styled-components"
import { GridContainer } from "../../utils/grid"

export const ProjectList = styled(GridContainer)`
	max-width: 50rem;
	grid-template-columns: repeat(2, 1fr);
	grid-gap: 2rem 5rem;
`

export const ProjectItem = styled.div`
	height: 14.5rem;
	width: 100%;
	cursor: pointer;
	position: relative;
	overflow: hidden;

	&:hover img {
		transform: scale(1.2);
	}
	&:hover div {
		bottom: 0;
	}
`

export const ProjectImage = styled.img`
	width: 100%;
	transition: all 0.5s;
`

export const ProjectText = styled.div`
	position: absolute;
	bottom: -40%;
	left: 0;
	width: 100%;
	padding: 1.5rem 0;
	text-align: center;
	background-color: rgba(247, 136, 0, 0.6);
	transition: all 0.5s;
	& > h4 {
		font-size: 2rem;
		font-weight: 300;
		letter-spacing: 0.2rem;
		color: #fff;
	}
`
