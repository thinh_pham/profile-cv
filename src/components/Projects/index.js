import React from "react"
import PropTypes from "prop-types"
import { ProjectList, ProjectItem, ProjectImage, ProjectText } from "./styles"

Projects.propTypes = {
	projects: PropTypes.array,
}

Projects.defaultProps = {
	projects: [],
}

export default function Projects(props) {
	const { projects } = props

	return (
		<>
			<h3 className="title-section">Dự án</h3>
			<ProjectList>
				{projects.map((project) => (
					<ProjectItem key={project.id}>
						<ProjectImage src={project.img} alt={project.name} />
						<ProjectText>
							<h4>{project.name}</h4>
						</ProjectText>
					</ProjectItem>
				))}
			</ProjectList>
		</>
	)
}
