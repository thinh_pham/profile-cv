import React from "react"
import { FaUser, FaEnvelope } from "react-icons/fa"
import { GridContainer } from "../../utils/grid"
import { ProfileGrid, IconBorder, Title, InfoDetails, InfoItem } from "./styles"

export default function ProfileInfo() {
	return (
		<GridContainer style={{ marginTop: "3rem" }}>
			<ProfileGrid rStart={1} cStart={1} rEnd={1} cEnd={5}>
				<IconBorder>
					<FaUser />
				</IconBorder>
				<Title>Thông tin cá nhân</Title>
				<InfoDetails>
					<InfoItem>
						<b>Ngày sinh</b> : 17-12-2000
					</InfoItem>
					<InfoItem>
						<b>Vị trí</b> : Front-end Developer
					</InfoItem>
					<InfoItem>
						<b>Sở thích</b> : Đọc sách và bơi lội
					</InfoItem>
				</InfoDetails>
			</ProfileGrid>
			<ProfileGrid rStart={1} cStart={5} rEnd={1} cEnd={13}>
				<IconBorder>
					<FaEnvelope />
				</IconBorder>
				<Title>Liên hệ</Title>
				<InfoDetails>
					<InfoItem>
						<b>E-mail</b> : thinhphamk8@gmail.com
					</InfoItem>
					<InfoItem>
						<b>Điện thoại</b> : (+84) 964 612 814
					</InfoItem>
					<InfoItem>
						<b>Địa chỉ</b> : 665/64/12, Đường số 17A, Bình Hưng Hòa A, Bình Tân, Tp. Hồ Chí Minh
					</InfoItem>
				</InfoDetails>
			</ProfileGrid>
		</GridContainer>
	)
}
