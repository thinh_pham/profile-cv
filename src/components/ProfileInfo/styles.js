import { Col } from "../../utils/grid"
import styled from "styled-components"

export const ProfileGrid = styled(Col)`
	padding: 2rem 0;
	align-self: start;
`

export const IconBorder = styled.span`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 3rem;
	height: 3.5rem;
	margin: 0 auto;
	padding: 0.5rem;
	text-align: center;
	color: #f98900;
	font-size: 1.8rem;
	border: 0.1rem solid #f98900;
	border-radius: 1.5rem 0;
`

export const Title = styled.h5`
	color: #f98900;
	font-size: 2rem;
	font-weight: 200;
	text-align: center;
	margin: 2rem 0;
`

export const InfoDetails = styled.ul``

export const InfoItem = styled.li`
	list-style: none;
	text-align: center;
	margin: 1rem 0;
	color: #fff;
	font-size: 1.3rem;
	line-height: 1.8rem;
	letter-spacing: 0.1rem;
	& > b {
		font-size: 1.4rem;
		text-transform: uppercase;
	}
`
