import React from "react"
import PropTypes from "prop-types"
import AccordionItem from "./AccordionItem"

Accordion.propTypes = {
	accordions: PropTypes.array,
}

Accordion.defaultProps = {
	accordions: [],
}

export default function Accordion(props) {
	const { accordions } = props
	return (
		<>
			{accordions.map((acc) => {
				return <AccordionItem key={acc.id} accordion={acc} />
			})}
		</>
	)
}
