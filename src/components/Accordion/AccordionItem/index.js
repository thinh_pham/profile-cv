import React, { useState } from "react"
import PropTypes from "prop-types"
import { AccordionWrapper, AccordionButton, Panel } from "./styles"
import { FaMinus, FaPlus } from "react-icons/fa"

AccordionItem.propTypes = {
	accordion: PropTypes.shape({
		title: PropTypes.string,
		panelTitle: PropTypes.string,
		panelParagraph: PropTypes.string,
	}),
}

AccordionItem.defaultProps = {
	accordion: {},
}

export default function AccordionItem(props) {
	const [active, setActive] = useState(false)
	const { title, panelTitle, panelParagraph } = props.accordion

	return (
		<AccordionWrapper>
			<AccordionButton
				active={active}
				onClick={() => {
					setActive(!active)
				}}>
				<span>{active ? <FaPlus /> : <FaMinus />}</span>
				{title}
			</AccordionButton>
			<Panel active={active}>
				<h4>{panelTitle}</h4>
				<p>{panelParagraph}</p>
			</Panel>
		</AccordionWrapper>
	)
}
