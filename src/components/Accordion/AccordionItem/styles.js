import styled from "styled-components"

export const AccordionWrapper = styled.div`
	margin: 1rem 0;
`

export const AccordionButton = styled.button`
	background-color: #fff;
	color: #f78800;
	font-size: 1.3rem;
	font-weight: bold;
	font-family: "Kurale", serif;
	text-align: left;
	cursor: pointer;
	padding: 1rem 2rem;
	width: 100%;
	border-radius: ${(props) => (props.active ? "0.5rem 0.5rem 0 0" : "0.5rem")};
	border: none;
	outline: none;
	transition: all 0.4s;
	& > span {
		font-size: 1rem;
		margin-right: 1rem;
		display: inline-block;
	}
`

export const Panel = styled.div`
	padding: 2rem;
	background-color: #efefef;
	border-radius: 0 0 0.5rem 0.5rem;
	box-shadow: 0 -1rem 0 0 #efefef;
	overflow: hidden;
	overflow: hidden;
	display: ${(props) => (props.active ? "block" : "none")};
	transition: all 0.4s 0s;
	& > h4 {
		font-size: 1.9rem;
		font-weight: 300;
		margin-bottom: 1.4rem;
	}
	& > p {
		font-size: 1.5rem;
		color: #999;
	}
`
