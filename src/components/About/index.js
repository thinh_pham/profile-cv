import React from "react"
import ProfileInfo from "../ProfileInfo"

export default function About() {
	return (
		<>
			<h3 className="title-section">giới thiệu</h3>
			<p className="paragraph-section">
				Anim proident amet quis consectetur amet occaecat Lorem exercitation laborum nulla
				laborum sunt non. Non quis dolore voluptate sunt cillum labore exercitation
				reprehenderit culpa. Occaecat non duis reprehenderit voluptate aute officia
				deserunt et. Dolore eiusmod culpa cupidatat nisi Lorem. Et nulla cillum
				adipisicing est qui consequat excepteur occaecat anim est excepteur.
			</p>
			<ProfileInfo />
		</>
	)
}
